import pandas as pd
import numpy as np
import copy
import itertools
from glob import glob
import seaborn as sns
import matplotlib.patches as mpatches
from collections import Counter
from collections import defaultdict
from itertools import combinations
import collections, functools, operator 
from bidict import bidict




class parameters():
    def __init__(self, nh, Whk, logPar=True, **kwargs):
        self.frcD = 0.007
        
        #EPI
        self.Beta = 1/5  # I --> N
        self.Nu = 1/6.4 # E --> I
        self.Gamma = 1/9.5  # N --> R
        self.Lambda = None # S --> E

        
        #MOBI
        self.m = 19.77
        
        #self.eta1 = 0.545599 #th50
        self.eta1 = 0.71713 #th60
        #self.eta1 = 0.768494 #th65
        #self.eta1 = 0.826559 #th70
        
        #self.mortality = np.array([6.49683*1e-5, 0.02921]) #th50
        self.mortality = np.array([0.000364335, 0.044003862]) #th60
        #self.mortality = np.array([0.0004521, 0.056]) #th65
        #self.mortality = np.array([0.00435433, 0.056]) #th70
        
        
        #self.ai = np.array([0.56967, 0.3257]) #th50
        self.ai = np.array([0.555039, 0.158731]) #th60
        #self.ai = np.array([0.5448620, 0.1485544]) #th65
        #self.ai = np.array([0.5098098, 0.189627]) #th70
        
        self.alpha_pre = 1
        self.alpha_lock = None
        
        self.b_pre = 0.09 
        self.b_lock = None
        
        self.nh = copy.copy(nh)
        self.Whk = copy.copy(Whk)
        
        #EXTRA
        self.precentageE = None
        self.precentageI = None
        
        # Override default parameters with user-supplied parameters
        for key, value in kwargs.items():
            assert key in vars(self), "Invalid parameter: " + key
            setattr(self, key, value)
        
        self.eta = np.array([self.eta1, 1-self.eta1])
        
        self.b = self.b_pre
        self.alpha = self.alpha_pre
    
        if logPar:
            self.precentageE = 10**self.precentageE
            self.precentageI = 10**self.precentageI
            
    def lockdown_b(self, partial):
        mask = np.zeros(self.Whk.shape).astype(bool)
        mask[partial] = True
        mask[:, partial] = True
        
        self.Whk[mask] *= self.b_lock
        np.fill_diagonal(self.Whk, 1 - (self.Whk.sum(1)-self.Whk.diagonal()))
        
        
        
        
        
def initialStates(p, initialCaseI=None, initialCaseE=None):
    """
    Create Intial Compartments
    p = parameters class
    """    
    shape = (p.ai.shape[0], p.Whk.shape[0])
    
    I = np.zeros(shape)
    E = np.zeros(shape)
    N = np.zeros(shape)
    R = np.zeros(shape)
    
    if initialCaseI is not None: 
        newI = (initialCaseI * p.precentageI)
        I = p.eta.reshape(-1,1).dot(newI.reshape(1,-1))
        
    if initialCaseE is not None:
        newE = initialCaseE * p.precentageE
        E = p.eta.reshape(-1,1).dot(newE.reshape(1,-1))
    
    Stot = np.ones(shape) * p.eta.reshape(-1,1).dot(p.nh.reshape(1,-1))
    S = Stot - E - I - N
    
    return S, E, I, N, R






def oneStep(SEINR,p):
    S, E, I, N, R = SEINR
    Lambda, Nu, Beta, Gamma = p.Lambda, p.Nu, p.Beta, p.Gamma
    m, eta, ai, alpha, nh, b, Whk = p.m, p.eta, p.ai, p.alpha, p.nh, p.b, p.Whk
    
    #aAvg = (eta*ai).sum()
    #nhTilde = (1 - b*alpha*aAvg)*nh + (b*alpha*aAvg * (Whk * (nh.reshape(-1, 1))).sum(0))
    aAvg = (eta*ai*alpha).sum()
    nhTilde = (1 - b*aAvg)*nh + (b*aAvg * (Whk * (nh.reshape(-1, 1))).sum(0))


    
    pht2 = (Whk * (((b * alpha * ai).reshape(-1, 1) * (I)).sum(0)).reshape(-1, 1)).sum(0)
    pht1 =  ((1-b*alpha*ai).reshape(-1, 1)  *  (I)).sum(0)
    ph = (1/nhTilde) * (pht1 + pht2)
    
    qht2 = (Whk * (((b * alpha * ai).reshape(-1, 1) * (I)).sum(0)).reshape(-1, 1)).sum(0)
    qht1 = (((1 - b)*alpha*ai).reshape(-1, 1)  *  (I)).sum(0)
    qh = (1/nhTilde) * (qht1 + qht2)
    
    
    PIt1 = (alpha * ai * (1-b) * Lambda * m).reshape(-1,1) * ph
    PIt2 = ((1 - (alpha * ai * b)) * Lambda * m).reshape(-1,1) * qh
    PIt3 = Lambda * m * ((alpha * ai * b).reshape(-1,1) * ((Whk * (ph + qh)).sum(1)))
    PI = PIt1 + PIt2 + PIt3
    
    
    # Evolve
    St1 = (1-PI)*S
    Et1 = (PI * S) + (1-Nu)*E
    It1 = Nu*E + (1-Beta)*I
    Nt1 = Beta*I + (1-Gamma)*N
    Rt1 = R + Gamma*N 
    
   
    S = St1
    E = Et1
    I = It1
    N = Nt1    
    R = Rt1
    
    if S.min().min() < 0: 
        E += np.minimum(0,S)
        S = np.maximum(0,S)
            
    if E.min().min() < 0: 
        I += np.minimum(0,E)
        E = np.maximum(0,E)
    
    if I.min().min() < 0: 
        N += np.minimum(0,I)
        I = np.maximum(0,I)

    if N.min().min() < 0: 
        R += np.minimum(0,N)
        N = np.maximum(0,N)
        
    R = np.maximum(0,R)

    
    #Check that each region population is preserved
    _nh = S.sum(0) + E.sum(0) + I.sum(0) + N.sum(0) + R.sum(0)
    assert np.isclose(nh, _nh, rtol=0, atol=1e-2).all(), f"population not preserved! {nh}, {_nh}"
    #_nh = np.round(_nh).astype(int)
    
    return S, E, I, N, R






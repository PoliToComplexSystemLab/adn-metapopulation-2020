import pandas as pd
import numpy as np
import copy
import itertools
from glob import glob
import seaborn as sns
import matplotlib.patches as mpatches
from collections import Counter
from collections import defaultdict
from itertools import combinations
import collections, functools, operator 
from bidict import bidict


def normalizeNameProvincie(a):
    if a == "Reggio di Calabria": a = "Reggio Calabria"
    if "Aosta" in a: a = "Valle d'Aosta"
    if "Bolzano" in a: a = "Bolzano"
    if "Trento" in a: a = "Trento"
    if "Trentino" in a: a = "Trentino"
    if "Friuli-Venezia Giulia" in a: a = "Friuli Venezia Giulia"
    if a == "Massa Carrara": a = "Massa-Carrara"
    return a



def readMorti(covidProvPath = None, covidRegionPath = None):
    if covidProvPath is None: covidProvPath = "./Data/Epidemia/dpc-covid19-ita-province.csv"
    if covidRegionPath is None: covidRegionPath = "./Data/Epidemia/dpc-covid19-ita-regioni.csv"   

    cvdRg = pd.read_csv(covidRegionPath)
    cvdRg.data = pd.to_datetime(cvdRg.data)
    cvdRgDc = cvdRg[["data", "denominazione_regione", "deceduti"]]

    cvdRgDc = cvdRgDc.set_index(["data", "denominazione_regione"]).unstack().resample('1D').max()
    cvdRgDc = cvdRgDc.droplevel(0, axis=1)

    cvdRgDc["Trentino"] = cvdRgDc[["P.A. Bolzano","P.A. Trento"]].sum(1)
    cvdRgDc = cvdRgDc.drop(["P.A. Bolzano","P.A. Trento"], axis=1)

    cvdRgDc_CUM = cvdRgDc
    cvdRgDc_CUM = cvdRgDc_CUM.copy()
    cvdRgDc_NEW = cvdRgDc_CUM.diff()
    
    return cvdRgDc_CUM, cvdRgDc_NEW
    
    

def readCovidDataset(covidProvPath = None, covidRegionPath = None, summerW=False):
    
    if covidProvPath is None: covidProvPath = "./Data/Epidemia/dpc-covid19-ita-province.csv"
    if covidRegionPath is None: covidRegionPath = "./Data/Epidemia/dpc-covid19-ita-regioni.csv"
        
    #############################################
    # MAP province to north-south-centre
    #############################################
    provincie = pd.read_csv("./Data/General/ProvincieRegioneClassificazione.csv", sep=";", header=[0,1])
    provincie = provincie.droplevel(0, axis=1)
    provincie = provincie[provincie.TIPO_UTS.isin(['Citta metropolitana', 'Libero consorzio di comuni', 'Provincia', 'Provincia autonoma', "Unità non amministrativa"])]
    provincie.DEN_UTS = provincie.DEN_UTS.apply(normalizeNameProvincie)
    dictProvincieNS = dict(provincie[["DEN_UTS", "DEN_RIP"]].to_numpy())

    
    #############################################
    # MAP region to north-south-centre
    #############################################
    provincie = pd.read_csv("./Data/General/ProvincieRegioneClassificazione.csv", sep=";", header=[0,1])
    provincie = provincie.droplevel(0, axis=1).drop_duplicates("DEN_REG")
    provincie.DEN_REG = provincie.DEN_REG.apply(normalizeNameProvincie)
    dictRegionNS = dict(provincie[["DEN_REG", "DEN_RIP"]].to_numpy())

    
    #############################################
    # Population and ordered province
    #############################################
    pop = pd.read_csv("./Data/FinalForCommuting/pop.csv", sep=",", index_col=None)
    orderedPROV = pop.Territorio.to_numpy()
    popPROV = pop.set_index("Territorio").Value.to_dict()
    indexPROV = bidict(zip(orderedPROV, range(len(orderedPROV))))
    nh = pop.Value.to_numpy()

    
    #############################################
    # Travel matrix
    #############################################
    if summerW: flowMatrix = pd.read_csv("./Data/FinalForCommuting/Total_RAW_SUMMER", index_col=0)
    else: flowMatrix = pd.read_csv("./Data/FinalForCommuting/Total_RAW", index_col=0)
        
    flowMatrix = flowMatrix.loc[orderedPROV, orderedPROV]
    Whk = flowMatrix.to_numpy()
    np.fill_diagonal(Whk, 0)
    Whk = (Whk / Whk.sum(1).reshape(-1, 1))

    
    #############################################
    # Dict Provincia --> Regione
    #############################################
    provincie = pd.read_csv("./Data/General/ProvincieRegioneClassificazione.csv", sep=";", header=[0,1])
    provincie = provincie.droplevel(0, axis=1)
    provincie = provincie[provincie.TIPO_UTS.isin(['Citta metropolitana', 'Libero consorzio di comuni', 'Provincia', 'Provincia autonoma', "Unità non amministrativa"])]
    provincie.DEN_UTS = provincie.DEN_UTS.apply(normalizeNameProvincie)
    provincie.DEN_REG = provincie.DEN_REG.apply(normalizeNameProvincie)
    convertPROV_REG = dict(provincie[["DEN_UTS", "DEN_REG"]].to_numpy())

    
    #############################################
    # Covid Dataset Provincie
    #############################################
    
    covidProv = pd.read_csv(covidProvPath)
    covidProv.denominazione_provincia = covidProv.denominazione_provincia.apply(normalizeNameProvincie)
    

    covidProv.data = pd.to_datetime(covidProv.data)
    covidProv = covidProv[["data", "denominazione_provincia", "totale_casi"]]
    covidProv = covidProv[~(covidProv.denominazione_provincia == "In fase di definizione/aggiornamento")]
    covidProv = covidProv[~(covidProv.denominazione_provincia == "Fuori Regione / Provincia Autonoma")]

    covidProv = covidProv.set_index(["data", "denominazione_provincia"]).unstack().resample('1D').max()
    covidProv = covidProv.droplevel(0, axis=1)

    covidProv = covidProv[orderedPROV]

    
    #############################################
    # Covid Dataset Regioni
    #############################################
    
    covidRegion = pd.read_csv(covidRegionPath)
    covidRegion.data = pd.to_datetime(covidRegion.data)
    covidRegion = covidRegion[["data", "denominazione_regione", "nuovi_positivi"]]

    covidRegion = covidRegion.set_index(["data", "denominazione_regione"]).unstack().resample('1D').max()
    covidRegion = covidRegion.droplevel(0, axis=1)


    # -- cumulate
    covidRegionCum = covidRegion.cumsum(axis=0)
    covidRegionCum["Trentino"] = covidRegionCum[["P.A. Bolzano","P.A. Trento"]].sum(1)
    covidRegionCum = covidRegionCum.drop(["P.A. Bolzano","P.A. Trento"], axis=1)


    # -- new I
    covidRegionNew = covidRegion
    covidRegionNew["Trentino"] = covidRegionNew[["P.A. Bolzano","P.A. Trento"]].sum(1)
    covidRegionNew = covidRegionNew.drop(["P.A. Bolzano","P.A. Trento"], axis=1)


    # -- actual I
    covidRegion = pd.read_csv("./Data/Epidemia/dpc-covid19-ita-regioni.csv")
    covidRegion.data = pd.to_datetime(covidRegion.data)
    covidRegion = covidRegion[["data", "denominazione_regione", "totale_positivi"]]
    orderedREG = list(set(covidRegion.denominazione_regione))

    covidRegion = covidRegion.set_index(["data", "denominazione_regione"]).unstack().resample('1D').max()
    covidRegion = covidRegion.droplevel(0, axis=1)

    covidRegion["Trentino"] = covidRegion[["P.A. Bolzano","P.A. Trento"]].sum(1)
    covidRegion = covidRegion.drop(["P.A. Bolzano","P.A. Trento"], axis=1)

    # Account for negative values
    covidRegionNew = covidRegionNew.applymap(lambda a: max(a,0))
    

    return dictProvincieNS, dictRegionNS, orderedPROV, convertPROV_REG, nh, Whk, covidProv, covidRegion, covidRegionCum, covidRegionNew
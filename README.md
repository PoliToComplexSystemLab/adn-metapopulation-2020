# ADN metapopulation 2020
The code used in the paper "Modelling and predicting the effect of social distancing and travel restrictions on COVID-19 spreading" (https://doi.org/10.1098/rsif.2020.0875).  
This standalone package contains the code and the dataset sufficient to reproduce all the quantitative results in the manuscript.

# Instructions
The code for the simulation is written in `PYTHON (Version: 3.7.7)` and can be used through ipython notebooks  
No further installations are required

### OS Requirements
The package has been tested on `MacOS 10.15 `and `Ubuntu 18.04.4 LTS`

### Dependencies
Code depends on the Python scientific stack:
```  
bidict == 0.21.2  
matplotlib == 3.3.0  
numpy == 1.19.1  
pandas == 1.1.3  
seaborn == 0.11.0 
```
  
### Data
To run the code on custom data you need to substitute the following dataset: the epidemic time-series in `/Data/epidemia`, the travel matrix between and the communities population in `/Data/FinalForCommuting`, and finally the classification of communities in macro-regions in `/Data/General`. 

# Organization
The `Covid_Lib_Simulator.py` file contains the epidemic progression as described in the manuscript.  
The `Covid_Lib_Data.py` reads the datasets provided in `/data` and should be adapted if used with custom datasets.  

The notebooks are organized as follow:
- 0_Calibration.ipynb: Code used to calibrate the model with the Italian COVID-19 outbreak 
- 1_LockdownScenario.ipynb: It generates Fig. 4 of the main text
- 2_LockdownScenario.ipynb: It generates Fig. 5 of the main text
- 3_Lockdown_TargetedLock.ipynb: It generates Fig. 3 of the main text
- 4_ReopenScenarios.ipynb: It generates Fig. 6 of the main text




## Demo
The demo notebook reproduces some of the results of the main text (specifically Fig. 3 and Fig. 5), regarding the lockdown scenarios, using the calibrated model. 
The demo already contains the calibrated model and reads data from the `data` folder in order to produce the results. 
The run time is of few seconds on a standard PC.



# Licence
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type"> ADN metapopulation 2020 </span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

